#!/usr/bin/env bash
# Swirls kernel compilation script
# GSL - 2015

######## CONFIG ########
# Android prebuild tools path
PREBUILD=/home/gabriel/distrib/android_dev/prebuilt
# AOSP path
AOSP=/home/gabriel/projects/swirls/taintdroid-4.1.1_r6
# Android device dir
DEVICE=device/asus/grouper
# Device target
TARGET=tegra3_android_defconfig
########################

# Setup compilation environment
export PATH=$PREBUILD/linux-x86/toolchain/arm-eabi-4.4.3/bin:$PATH
export ARCH=arm
export SUBARCH=arm
export CROSS_COMPILE=arm-eabi-

# Configure kernel for target
make $TARGET

# Compile kernel
#make -j16 CONFIG_DEBUG_SECTION_MISMATCH=y CONFIG_SECURITY_TAINTCTRL=y #CONFIG_SECURITY=y
make -j16 CONFIG_DEBUG_SECTION_MISMATCH=y CONFIG_UNION_FS=y CONFIG_UNION_FS_XATTR=y

# backup previous version
echo "Backup of the current kernel version..."
date=`date +%Y%m%d%H%M`
cp $AOSP/$DEVICE/kernel $AOSP/$DEVICE/kernel_backup_$date

echo "Installing the new kernel version..."
# install new kernel
cp arch/arm/boot/zImage $AOSP/$DEVICE/kernel
