/*
 *  arch/arm/kernel/taint.c
 * 
 */

#include <linux/syscalls.h>
#include <linux/types.h>
#include <linux/sched.h>
#include <linux/slab.h>

#include <linux/module.h>
#include <linux/kernel.h>
#include <net/genetlink.h>

#include "taintctrl.h"
/*
//STRUCTS//////////////////////////////////////////////////////////////////////
static struct security_operations taintctrl_ops = {
        .name =                         "taintctrl",
#if 0 
        .ptrace_access_check            =               taintctrl_ptrace_access_check,
        .ptrace_traceme                         =               taintctrl_ptrace_traceme,
        .capget                                         =               taintctrl_capget,
        .capset                                         =               taintctrl_capset,
        .capable                                        =               taintctrl_capable,
        .quotactl                                       =               taintctrl_quotactl,
        .quota_on                                       =               taintctrl_quota_on,
        .syslog                                         =               taintctrl_syslog,
        .vm_enough_memory                       =               taintctrl_vm_enough_memory,
 
        .netlink_send                           =               taintctrl_netlink_send,
        .netlink_recv                           =               taintctrl_netlink_recv,
 
        .bprm_set_creds                         =               taintctrl_bprm_set_creds,
        .bprm_committing_creds          =               taintctrl_bprm_committing_creds,
        .bprm_committed_creds           =               taintctrl_bprm_committed_creds,
        .bprm_secureexec                        =               taintctrl_bprm_secureexec,
 
        .sb_alloc_security                      =               taintctrl_sb_alloc_security,
        .sb_free_security                       =               taintctrl_sb_free_security,
        .sb_copy_data                           =               taintctrl_sb_copy_data,
        .sb_remount                             =               taintctrl_sb_remount,
        .sb_kern_mount                          =               taintctrl_sb_kern_mount,
        .sb_show_options                        =               taintctrl_sb_show_options,
        .sb_statfs                                      =               taintctrl_sb_statfs,
        .sb_mount                                       =               taintctrl_mount,
        .sb_umount                                      =               taintctrl_umount,
        .sb_set_mnt_opts                        =               taintctrl_set_mnt_opts,
        .sb_clone_mnt_opts                      =               taintctrl_sb_clone_mnt_opts,
        .sb_parse_opts_str                      =               taintctrl_parse_opts_str,
 
        .inode_alloc_security           =               taintctrl_inode_alloc_security,
        .inode_free_security            =               taintctrl_inode_free_security,
        .inode_init_security            =               taintctrl_inode_init_security,
        .inode_create                           =               taintctrl_inode_create,
        .inode_link                             =               taintctrl_inode_link,
        .inode_unlink                           =               taintctrl_inode_unlink,
        .inode_symlink                          =               taintctrl_inode_symlink,
        .inode_mkdir                            =               taintctrl_inode_mkdir,
        .inode_rmdir                            =               taintctrl_inode_rmdir,
        .inode_mknod                            =               taintctrl_inode_mknod,
        .inode_rename                           =               taintctrl_inode_rename,
        .inode_readlink                         =               taintctrl_inode_readlink,
        .inode_follow_link                      =               taintctrl_inode_follow_link,
        .inode_permission                       =               taintctrl_inode_permission,
        .inode_setattr                          =               taintctrl_inode_setattr,
        .inode_getattr                          =               taintctrl_inode_getattr,
        .inode_setxattr                         =               taintctrl_inode_setxattr,
        .inode_post_setxattr            =               taintctrl_inode_post_setxattr,
        .inode_getxattr                         =               taintctrl_inode_getxattr,
        .inode_listxattr                        =               taintctrl_inode_listxattr,
        .inode_removexattr                      =               taintctrl_inode_removexattr,
        .inode_getsecurity                      =               taintctrl_inode_getsecurity,
        .inode_setsecurity                      =               taintctrl_inode_setsecurity,
        .inode_listsecurity             =               taintctrl_inode_listsecurity,
        .inode_getsecid                         =               taintctrl_inode_getsecid,
#endif
        .file_permission                        =               taintctrl_file_permission,
#if 0
        .file_alloc_security            =               taintctrl_file_alloc_security,
        .file_free_security             =               taintctrl_file_free_security,
        .file_ioctl                             =               taintctrl_file_ioctl,
        .file_mmap                                      =               taintctrl_file_mmap,
        .file_mprotect                          =               taintctrl_file_mprotect,
        .file_lock                                      =               taintctrl_file_lock,
        .file_fcntl                             =               taintctrl_file_fcntl,
        .file_set_fowner                        =               taintctrl_file_set_fowner,
        .file_send_sigiotask            =               taintctrl_file_send_sigiotask,
        .file_receive                           =               taintctrl_file_receive,
       
        .dentry_open                            =               taintctrl_dentry_open,
 
        .task_create                            =               taintctrl_task_create,
        .cred_alloc_blank                       =               taintctrl_cred_alloc_blank,
        .cred_free                                      =               taintctrl_cred_free,
        .cred_prepare                           =               taintctrl_cred_prepare,
        .cred_transfer                          =               taintctrl_cred_transfer,
        .kernel_act_as                          =               taintctrl_kernel_act_as,
        .kernel_create_files_as         =               taintctrl_kernel_create_files_as,
        .kernel_module_request          =               taintctrl_kernel_module_request,
        .task_setpgid                           =               taintctrl_task_setpgid,
        .task_getpgid                           =               taintctrl_task_getpgid,
        .task_getsid                            =               taintctrl_task_getsid,
        .task_getsecid                          =               taintctrl_task_getsecid,
        .task_setnice                           =               taintctrl_task_setnice,
        .task_setioprio                         =               taintctrl_task_setioprio,
        .task_getioprio                         =               taintctrl_task_getioprio,
        .task_setrlimit                         =               taintctrl_task_setrlimit,
        .task_setscheduler                      =               taintctrl_task_setscheduler,
        .task_getscheduler                      =               taintctrl_task_getscheduler,
        .task_movememory                        =               taintctrl_task_movememory,
        .task_kill                                      =               taintctrl_task_kill,
        .task_wait                                      =               taintctrl_task_wait,
        .task_to_inode                          =               taintctrl_task_to_inode,
 
        .ipc_permission                         =               taintctrl_ipc_permission,
        .ipc_getsecid                           =               taintctrl_ipc_getsecid,
 
        .msg_msg_alloc_security         =               taintctrl_msg_msg_alloc_security,
        .msg_msg_free_security          =               taintctrl_msg_msg_free_security,
 
        .msg_queue_alloc_security       =               taintctrl_msg_queue_alloc_security,
        .msg_queue_free_security        =               taintctrl_msg_queue_free_security,
        .msg_queue_associate            =               taintctrl_msg_queue_associate,
        .msg_queue_msgctl                       =               taintctrl_msg_queue_msgctl,
        .msg_queue_msgsnd                       =               taintctrl_msg_queue_msgsnd,
        .msg_queue_msgrcv                       =               taintctrl_msg_queue_msgrcv,
 
        .shm_alloc_security             =               taintctrl_shm_alloc_security,
        .shm_free_security                      =               taintctrl_shm_free_security,
        .shm_associate                          =               taintctrl_shm_associate,
        .shm_shmctl                             =               taintctrl_shm_shmctl,
        .shm_shmat                                      =               taintctrl_shm_shmat,
 
        .sem_alloc_security             =               taintctrl_sem_alloc_security,
        .sem_free_security                      =               taintctrl_sem_free_security,
        .sem_associate                          =               taintctrl_sem_associate,
        .sem_semctl                             =               taintctrl_sem_semctl,
        .sem_semop                                      =               taintctrl_sem_semop,
 
        .d_instantiate                          =               taintctrl_d_instantiate,
 
        .getprocattr                            =               taintctrl_getprocattr,
        .setprocattr                            =               taintctrl_setprocattr,
 
        .secid_to_secctx                        =               taintctrl_secid_to_secctx,
        .secctx_to_secid                        =               taintctrl_secctx_to_secid,
        .release_secctx                         =               taintctrl_release_secctx,
        .inode_notifysecctx             =               taintctrl_inode_notifysecctx,
        .inode_setsecctx                        =               taintctrl_inode_setsecctx,
        .inode_getsecctx                        =               taintctrl_inode_getsecctx,
 
        .unix_stream_connect            =               taintctrl_socket_unix_stream_connect,
        .unix_may_send                          =               taintctrl_socket_unix_may_send,
 
        .socket_create                          =               taintctrl_socket_create,
        .socket_post_create             =               taintctrl_socket_post_create,
        .socket_bind                            =               taintctrl_socket_bind,
        .socket_connect                         =               taintctrl_socket_connect,
        .socket_listen                          =               taintctrl_socket_listen,
        .socket_accept                          =               taintctrl_socket_accept,
        .socket_sendmsg                         =               taintctrl_socket_sendmsg,
        .socket_recvmsg                         =               taintctrl_socket_recvmsg,
        .socket_getsockname             =               taintctrl_socket_getsockname,
        .socket_getpeername                     =               taintctrl_socket_getpeername,
        .socket_getsockopt                      =               taintctrl_socket_getsockopt,
        .socket_setsockopt                      =               taintctrl_socket_setsockopt,
        .socket_shutdown                        =               taintctrl_socket_shutdown,
        .socket_sock_rcv_skb            =               taintctrl_socket_sock_rcv_skb,
        .socket_getpeersec_stream       =               taintctrl_socket_getpeersec_stream,
        .socket_getpeersec_dgram        =               taintctrl_socket_getpeersec_dgram,
        .sk_alloc_security                      =               taintctrl_sk_alloc_security,
        .sk_free_security                       =               taintctrl_sk_free_security,
        .sk_clone_security                      =               taintctrl_sk_clone_security,
        .sk_getsecid                            =               taintctrl_sk_getsecid,
        .sock_graft                             =               taintctrl_sock_graft,
        .inet_conn_request                      =               taintctrl_inet_conn_request,
        .inet_csk_clone                         =               taintctrl_inet_csk_clone,
        .inet_conn_established          =               taintctrl_inet_conn_established,
        .secmark_relabel_packet         =               taintctrl_secmark_relabel_packet,
        .secmark_refcount_inc           =               taintctrl_secmark_refcount_inc,
        .secmark_refcount_dec           =               taintctrl_secmark_refcount_dec,
        .req_classify_flow                      =               taintctrl_req_classify_flow,
        .tun_dev_create                         =               taintctrl_tun_dev_create,
        .tun_dev_post_create            =               taintctrl_tun_dev_post_create,
        .tun_dev_attach                         =               taintctrl_tun_dev_attach,
#endif
};
*/

enum TAG_RESPONSE {
    TAG_NONEXISTANT,
    TAG_BLOCK,
    TAG_ALLOW,
    TAG_ALLOW_LOG,
    TAG_UPDATE
};

struct tag_policy_t {
    unsigned int tag_caller;
    unsigned int tag_receiver;
    enum TAG_RESPONSE response;
};

struct tag_policies {
    struct tag_policy_t ** pol;
    int n;
};

struct tag_policies tags;

//ABOUT////////////////////////////////////////////////////////////////////////

MODULE_AUTHOR("gabriel");
MODULE_DESCRIPTION("Taintctrl Linux Security Module");
MODULE_LICENSE("GPL");
MODULE_VERSION("0.1");

// return the taint of a process task_struct-> tag
int gettaint(pid_t pid)
{
    /*
    struct task_struct * task;
    task = find_task_by_vpid(pid);
    return task->tag;
    */
}

// set the taint of a process task_struct-> tag
int settaint(pid_t pid, unsigned int tag)
{
    /*
    struct task_struct * task;
    task = find_task_by_vpid(pid);
    task->tag = tag;
//    printk("############## SETTAINT FOR PROCESS %i WITH TAINT %i ##################\n", pid, tag);
    return task->tag;
    */
}

int load_initial_policy(void){

    tags.n = 3;
    tags.pol = kmalloc(tags.n * sizeof(struct tag_policy_t *), GFP_KERNEL);

    /* 1 cannot talk to 2 */
    tags.pol[0]=(struct tag_policy_t*) kmalloc(sizeof(struct tag_policy_t), GFP_KERNEL);
    tags.pol[0]->tag_caller = 1;
    tags.pol[0]->tag_receiver = 2;
    tags.pol[0]->response = TAG_ALLOW_LOG;

    /* 2 cannot talk to 1 */
    tags.pol[1]=(struct tag_policy_t*) kmalloc(sizeof(struct tag_policy_t), GFP_KERNEL);
    tags.pol[1]->tag_caller = 2;
    tags.pol[1]->tag_receiver = 1;
    tags.pol[1]->response = TAG_ALLOW_LOG;

    /* 3 cannot talk to 4 */
    tags.pol[2]=(struct tag_policy_t*) kmalloc(sizeof(struct tag_policy_t), GFP_KERNEL);
    tags.pol[2]->tag_caller = 3;
    tags.pol[2]->tag_receiver = 4;
    tags.pol[2]->response = TAG_ALLOW_LOG;
    
    return tags.n;
}



// implement a tag based policy
// return 1 if process can communicate
// return 0 if they can't
int checkpolicy(int tag1, int tag2)
{

    enum TAG_RESPONSE r;
    int i;
    
    /* by default we don't have any policy to apply */
    r = TAG_NONEXISTANT;

    if (tag1 == tag2){
        r = TAG_ALLOW_LOG;
        goto return_pol;
    }

    /* calling process with a color, receiver not tainted */
/*    if (tag1 && !tag2){
        r = TAG_UPDATE;
        goto return_pol;
    }
*/

    /* checking the policy */
    for (i=0; i < tags.n; i++) {
        //printk("Checking entry % in the policy structure\n", i);
        if ((tags.pol[i]->tag_caller == tag1) 
         && (tags.pol[i]->tag_receiver == tag2)){
            r = tags.pol[i]->response;
            goto return_pol;
        }
    }
    goto return_pol;

return_pol:
    return r;
}

SYSCALL_DEFINE0(loadpolicy)
{
return load_initial_policy();
}

SYSCALL_DEFINE1(gettaint, pid_t, pid)
{
    return gettaint(pid);
}

SYSCALL_DEFINE2(settaint, pid_t, pid, unsigned int, tag)
{
    return settaint(pid, tag);
}


// Netlink socket approach
//#######################################################################

/* attributes (variables): the index in this enum is used as a reference for the type,
 *             userspace application has to indicate the corresponding type
 *             the policy is used for security considerations 
 */
enum {
    TAINTCTRL_A_UNSPEC,
    TAINTCTRL_A_MSG,
    __TAINTCTRL_A_MAX,
};
#define TAINTCTRL_A_MAX (__TAINTCTRL_A_MAX - 1)

/* attribute policy: defines which attribute has which type (e.g int, char * etc)
 * possible values defined in net/netlink.h 
 */
static struct nla_policy taintctrl_genl_policy[TAINTCTRL_A_MAX + 1] = {
    [TAINTCTRL_A_MSG] = { .type = NLA_NUL_STRING },
};

#define VERSION_NR 1
/* family definition */
static struct genl_family taintctrl_gnl_family = {
    .id = GENL_ID_GENERATE,         //genetlink should generate an id
    .hdrsize = 0,
    .name = "TAINTCTRL",        //the name of this family, used by userspace application
    .version = VERSION_NR,          //version number  
    .maxattr = TAINTCTRL_A_MAX,
};

/* commands: enumeration of all commands (functions), 
 * used by userspace application to identify command to be ececuted
 */
enum {
    TAINTCTRL_C_UNSPEC,
    TAINTCTRL_C_FILETAINT,
    TAINTCTRL_C_PROCESSTAINT,
    __TAINTCTRL_C_MAX,
};
#define TAINTCTRL_C_MAX (__TAINTCTRL_C_MAX - 1)


/* an echo command, receives a message, prints it and sends another message back */
int taint_req(struct sk_buff *skb_2, struct genl_info *info)
{
        struct nlattr *na;
        struct sk_buff *skb;
        int rc;
    void *msg_head;
    char * mydata;
    
        if (info == NULL)
                goto out;
  
        /*for each attribute there is an index in info->attrs which points to a nlattr structure
         *in this structure the data is given
         */
        na = info->attrs[TAINTCTRL_A_MSG];
        if (na) {
        mydata = (char *)nla_data(na);
        if (mydata == NULL)
            printk("error while receiving data\n");
        else
            printk("received: %s\n", mydata);
        }
    else
        printk("no info->attrs %i\n", TAINTCTRL_A_MSG);

        /* send a message back*/
        /* allocate some memory, since the size is not yet known use NLMSG_GOODSIZE*/   
        skb = genlmsg_new(NLMSG_GOODSIZE, GFP_KERNEL);
    if (skb == NULL)
        goto out;

    /* create the message headers */
        /* arguments of genlmsg_put: 
           struct sk_buff *, 
           int (sending) pid, 
           int sequence number, 
           struct genl_family *, 
           int flags, 
           u8 command index (why do we need this?)
        */
        msg_head = genlmsg_put(skb, 0, info->snd_seq+1, &taintctrl_gnl_family, 0, TAINTCTRL_C_FILETAINT);
    if (msg_head == NULL) {
        rc = -ENOMEM;
        goto out;

    }
    /* add a TAINTCTRL_A_MSG attribute (actual value to be sent) */
    rc = nla_put_string(skb, TAINTCTRL_A_MSG, "hello world from kernel space");
    if (rc != 0)
        goto out;
    
        /* finalize the message */
    genlmsg_end(skb, msg_head);

        /* send the message back */
    rc = genlmsg_unicast(genl_info_net(info), skb, info->snd_pid );
    if (rc != 0)
        goto out;
    return 0;

 out:
        printk("an error occured in taint_req:\n");
  
      return 0;
}

// used to resolve taint associated to a file in userspace
int taintctrl_filetaint_res(struct sk_buff *skb_2, struct genl_info *info)
{
    return  taint_req(skb_2, info);
}

/* commands: mapping between the command enumeration and the actual function*/
struct genl_ops taintctrl_gnl_ops_echo = {
    .cmd = TAINTCTRL_C_FILETAINT,
    .flags = 0,
    .policy = taintctrl_genl_policy,
/*    .doit = taint_req,*/
    .doit = taintctrl_filetaint_res,
    .dumpit = NULL,
};

//INIT/////////////////////////////////////////////////////////////////////////
static int __init taintctrl_init(void)
{	
//    if (register_security(&taintctrl_ops)) 
//	{
//		panic(KERN_INFO "Failed to register taintctrl module\n");
//	}
    
    printk(KERN_ALERT "taintctrl started");
	
    return 0;

    int rc;
        printk("INIT GENERIC NETLINK TAINT MODULE\n");
        
        /*register new family*/
    rc = genl_register_family(&taintctrl_gnl_family);
    if (rc != 0)
        goto failure;
        /*register functions (commands) of the new family*/
    rc = genl_register_ops(&taintctrl_gnl_family, &taintctrl_gnl_ops_echo);
    if (rc != 0){
                printk("register ops: %i\n",rc);
                genl_unregister_family(&taintctrl_gnl_family);
        goto failure;
        }

    return 0;
    
  failure:
        printk("an error occured while inserting the taintctrl module\n");
    return -1;
    
}

static void __exit taintctrl_exit(void)
{
        int ret;
        printk("EXIT GENERIC NETLINK EXEMPLE MODULE\n");
        /*unregister the functions*/
    ret = genl_unregister_ops(&taintctrl_gnl_family, &taintctrl_gnl_ops_echo);
    if(ret != 0){
                printk("unregister ops: %i\n",ret);
                return;
        }
        /*unregister the family*/
    ret = genl_unregister_family(&taintctrl_gnl_family);
    if(ret !=0){
                printk("unregister family %i\n",ret);
        }
}

//MACROSES/////////////////////////////////////////////////////////////////////
//EXPORT_SYMBOL(gettaint);
//EXPORT_SYMBOL(settaint);
//EXPORT_SYMBOL(checkpolicy);
//EXPORT_SYMBOL(sys_gettaint);
//EXPORT_SYMBOL(sys_settaint);
//EXPORT_SYMBOL(sys_loadpolicy);
module_init(taintctrl_init);
module_exit(taintctrl_exit);
MODULE_LICENSE("GPL");

