/*
 *  arch/arm/kernel/taint.c
 *
 */

/*
 * Inspiration:
 *  http://www.cs.usfca.edu/~cruse/cs635/stash.c
 *  http://www.cs.usfca.edu/~cruse/cs635/
    http://people.ee.ethz.ch/~arkeller/linux/code/ioctl.c
 *
 */

// time evaluation
#include <linux/time.h>

#include <linux/syscalls.h>
#include <linux/types.h>
#include <linux/sched.h>
#include <linux/slab.h>

// Char dev include
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <asm/uaccess.h>

// tomoyo includes
#include <linux/types.h>
#include <linux/mount.h>
#include <linux/mnt_namespace.h>
#include <linux/fs_struct.h>
#include <linux/magic.h>
#include <linux/slab.h>
#include <net/sock.h>
//#include "../../../security/tomoyo/common.h"
#include "../../../fs/internal.h"

// Reading and writing include files
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/syscalls.h>
#include <linux/fcntl.h>
#include <asm/uaccess.h>

// mutexes
#include <linux/mutex.h>
#include <linux/completion.h>

// lists & hashs
#include <linux/list.h>
#include <linux/hash.h>

// read files
#include <linux/file.h>
#include <linux/splice.h>

#include <linux/tag.h>

// Access proc-fs
//#include <linux/proc_fs.h>
#include <linux/mm.h>

//extern long do_tee(struct file *in, struct file *out, size_t len, unsigned int flags);

// File tag cache list
struct open_file {
	struct list_head list;
	struct inode *inode;
	char *mnt_devname;
	char *path;
	int count;
	struct tag_policy_t pol;
	int has_changed;
};

// Userspace defined structure
struct capsule_t_u {
	int64_t idcapsule;
	char *name;
	int tag;
	int version;
};

struct context_t_u {
	int64_t idcontext;
	char * type;
	enum GEOFENCE_STATUS geoFenceStatus;
	enum TIMEFENCE_STATUS timeFenceStatus;
	enum OOC_ACTION outofcontextaction;
};

struct policy_t_u {
	int64_t from;
	int64_t to;
	enum TAG_RESPONSE action;
};

struct object_t_u {
	int tag;
	int persist; // for application only, means cannot start as different color
	int64_t idcontext;
	char *name;
};

// A linked list of whitelisted process you don't want to taint
struct whitelist_entry {
	struct list_head list;
	int pid;
};

/* objects and policies list heads */
struct open_file *ofs;
struct capsule_t *cap;
struct context_t *ctx;
struct policy_t *pol;
struct object_t *app;
struct object_t *procnexttaint;
struct object_t *sock;
struct whitelist_entry *wl;

/* char dev variables */
static int major;
static struct class* class = NULL;
static struct device* device = NULL;

#define TAG_MSG_SIZE 300
static char *msg;

/* semaphores & completion for all operations */
struct semaphore tag_sem_user;
struct semaphore tag_sem_kernel;
//struct completion comp_r, comp_w;

DEFINE_SEMAPHORE(tag_sem_user);
DEFINE_SEMAPHORE(tag_sem_kernel);
//sema_init(&tag_sem_user, 1);
//sema_init(&tag_sem_kernel, 1);

int request_pending = 0;
int response_received = 0;

/* wait queues */
wait_queue_head_t tag_poll;
wait_queue_head_t tag_wq;

// concurrency/synchro management
int system_ready = 0;
pid_t taintctrl_pid;

// DEBUG
#define SWIRLS_DEBUG

//ABOUT////////////////////////////////////////////////////////////////////////

MODULE_AUTHOR("gabriel");
MODULE_DESCRIPTION("Taintctrl Module");
MODULE_LICENSE("GPL");
MODULE_VERSION("0.1");

//FILE UTILITIES FROM TOMOYO /////////////////////////////////////////////////

char *tomoyo_encode(const char *str)
{
	int len = 0;
	const char *p = str;
	char *cp;
	char *cp0;

	if (!p)
		return NULL;
	while (*p) {
		const unsigned char c = *p++;
		if (c == '\\')
			len += 2;
		else if (c > ' ' && c < 127)
			len++;
		else
			len += 4;
	}
	len++;
	/* Reserve space for appending "/". */
	cp = kzalloc(len + 10, GFP_NOFS);
	if (!cp)
		return NULL;
	cp0 = cp;
	p = str;
	while (*p) {
		const unsigned char c = *p++;

		if (c == '\\') {
			*cp++ = '\\';
			*cp++ = '\\';
		} else if (c > ' ' && c < 127) {
			*cp++ = c;
		} else {
			*cp++ = '\\';
			*cp++ = (c >> 6) + '0';
			*cp++ = ((c >> 3) & 7) + '0';
			*cp++ = (c & 7) + '0';
		}
	}
	return cp0;
}

/**
 * tomoyo_realpath_from_path - Returns realpath(3) of the given pathname but ignores chroot'ed root.
 *
 * @path: Pointer to "struct path".
 *
 * Returns the realpath of the given @path on success, NULL otherwise.
 *
 * If dentry is a directory, trailing '/' is appended.
 * Characters out of 0x20 < c < 0x7F range are converted to
 * \ooo style octal string.
 * Character \ is converted to \\ string.
 *
 * These functions use kzalloc(), so the caller must call kfree()
 * if these functions didn't return NULL.
 */
char *tomoyo_realpath_from_path(struct path *path)
{
	char *buf = NULL;
	char *name = NULL;
	unsigned int buf_len = PAGE_SIZE / 2;
	struct dentry *dentry = path->dentry;
	bool is_dir;
	if (!dentry)
		return NULL;
	is_dir = dentry->d_inode && S_ISDIR(dentry->d_inode->i_mode);
	while (1) {
		char *pos;
		buf_len <<= 1;
		kfree(buf);
		buf = kmalloc(buf_len, GFP_NOFS);
		if (!buf)
			break;
		/* GSL code: discard sockets & pipes */
		if (dentry->d_sb && dentry->d_sb->s_magic == SOCKFS_MAGIC)
			return NULL;
		if (dentry->d_op && dentry->d_op->d_dname)
			return NULL;
		/* Get better name for socket. */
		/*
		   if (dentry->d_sb && dentry->d_sb->s_magic == SOCKFS_MAGIC) {
		   struct inode *inode = dentry->d_inode;
		   struct socket *sock = inode ? SOCKET_I(inode) : NULL;
		   struct sock *sk = sock ? sock->sk : NULL;
		   if (sk) {
		   snprintf(buf, buf_len - 1, "socket:[family=%u:"
		   "type=%u:protocol=%u]", sk->sk_family,
		   sk->sk_type, sk->sk_protocol);
		   } else {
		   snprintf(buf, buf_len - 1, "socket:[unknown]");
		   }
		   name = tomoyo_encode(buf);
		   break;
		   }
		   */
		/* For "socket:[\$]" and "pipe:[\$]". */
		/*
		   if (dentry->d_op && dentry->d_op->d_dname) {
		   pos = dentry->d_op->d_dname(dentry, buf, buf_len - 1);
		   if (IS_ERR(pos))
		   continue;
		   name = tomoyo_encode(pos);
		   break;
		   }
		   */
		/* If we don't have a vfsmount, we can't calculate. */
		if (!path->mnt)
			break;
		pos = d_absolute_path(path, buf, buf_len - 1);
		/* If path is disconnected, use "[unknown]" instead. */
		if (pos == ERR_PTR(-EINVAL)) {
			name = tomoyo_encode(NULL);
			break;
		}
		/* Prepend "/proc" prefix if using internal proc vfs mount. */
		if (!IS_ERR(pos) && (path->mnt->mnt_flags & MNT_INTERNAL) &&
				(path->mnt->mnt_sb->s_magic == PROC_SUPER_MAGIC)) {
			//return NULL;
			printk("TMP file here\n");
			pos -= 5;
			if (pos >= buf)
				memcpy(pos, "/proc", 5);
			else
				pos = ERR_PTR(-ENOMEM);
		}
		if (IS_ERR(pos))
			continue;
		name = tomoyo_encode(pos);
		break;
	}
	kfree(buf);
	if (!name)
	{
		return NULL;
		//tomoyo_warn_oom(__func__);
	}
	else if (is_dir && *name) {
		/* Append trailing '/' if dentry is a directory. */
		char *pos = name + strlen(name) - 1;
		if (*pos != '/')
			/*
			 * This is OK because tomoyo_encode() reserves space
			 * for appending "/".
			 */
			*++pos = '/';
	}
	return name;
}

int get_tag_pol_from_pid(pid_t pid)
{
	struct task_struct *task;
	task = find_task_by_vpid(pid);
	return task->idcap;
}


int gettaint(pid_t pid)
{
	struct task_struct *task;
	task = find_task_by_vpid(pid);
	if ( task == NULL ){
		return -1;
	}
	//printk("gettaint on process %li with id: %i\n", pid, task->idcap);
	return task->idcap;
}

int settaint(pid_t pid, unsigned int tag)
{
	struct task_struct *task, *thread;
	printk("settaint on process %li with id: %i\n", (long int) pid, tag);
	task = find_task_by_vpid(pid);
	if ( task == NULL ){
		return -1;
	}
	task->idcap = tag;
	thread = task;
	do {
		thread->idcap = tag;
	}
	while_each_thread(task, thread);
	return tag;
}

void add_to_capsule(struct capsule_t_u *uc)
{
	struct capsule_t * c = (struct capsule_t*) kmalloc(sizeof(struct capsule_t), GFP_KERNEL);
	c->id = uc->idcapsule;
	c->tag = uc->tag;
    c->name = uc->name;
    c->version = uc->version;

	list_add (&c->list, &cap->list);
}

void add_to_context(struct context_t_u *uc){
		struct context_t * c = (struct context_t*) kmalloc(sizeof(struct capsule_t), GFP_KERNEL);
		c->idcontext = uc->idcontext;
		c->geoFenceStatus = uc->geoFenceStatus;
		c->timeFenceStatus = uc->timeFenceStatus;
		c->outofcontextaction = uc->outofcontextaction;

		list_add(&c->list, &ctx->list);
}

void print_policy(void)
{

	//enum TAG_RESPONSE r;
	struct list_head *pos;
	struct policy_t *tmp;

	printk("Swirls: printing current taint status\n");
	list_for_each(pos, &pol->list){
		tmp = list_entry(pos, struct policy_t, list);
		printk("From taint %lli to %lli: \t %d\n", (long long int)tmp->from, (long long int)tmp->to, tmp->action);
	}
}

void add_to_policy(struct policy_t_u *up)
{
	struct policy_t * p = (struct policy_t*) kmalloc(sizeof(struct policy_t), GFP_KERNEL);
	p->from = up->from;
	p->to = up->to;
	p->action = up->action;

	list_add (&p->list, &pol->list);
	print_policy();
}

void set_next_process_taint(struct object_t_u *uobj)
{
	//char *name;//, *del;
	//int retval;
	struct list_head *pos;
	struct object_t *tmp, *a;

	if (! uobj->persist) {
		list_for_each(pos, &procnexttaint->list){
			tmp = list_entry(pos, struct object_t, list);
			// A matching application?
			if (! strcmp(uobj->name, tmp->name)) {
				// ... with a persistant taint?
				if (tmp->persist)
					return;
			}
		}
	}

	a = (struct object_t *) kmalloc(sizeof(struct object_t), GFP_KERNEL);
	a->tag = uobj->tag;
	a->name = (char*) kmalloc(strlen(uobj->name) +1, GFP_KERNEL);
	strncpy(a->name, uobj->name, strlen(uobj->name) +1);
	a->idcontext = uobj->idcontext;
	a->persist = uobj->persist;

	/*del = strsep(&msg, " ");
	name = (char*) kmalloc(strlen(del), GFP_KERNEL);
	*(del-1) = '\0'; // not usefull, kstrtoint should check if the char is relevant for an int conversion
	retval = kstrtoint(msg, 10, &a->tag);
	strncpy(name, del, strlen(del));
	a->name = name;
	*/
	printk("\t\t\t******** Setting taint %d on package %s\n", a->tag, a->name);

	list_for_each(pos, &procnexttaint->list){
		tmp = list_entry(pos, struct object_t, list);
		if (! strcmp(uobj->name, tmp->name)){
			// A matching application, shouldn't happen
			// But in that case, change the process to come color
			tmp->tag = a->tag;
			kfree(a->name);
			kfree(a);
			return;
		}
	}
	list_add (&a->list, &procnexttaint->list);
	return;
}

void get_next_process_taint(struct object_t_u *uobj)
{
	struct list_head *pos;
	struct object_t *tmp;

	printk("\t\t\t******** getting taint for package %s\n", uobj->name);
	list_for_each(pos, &procnexttaint->list){
		tmp = list_entry(pos, struct object_t, list);
		if (! strcmp(uobj->name, tmp->name)){
			// A matching application
			uobj->tag = tmp->tag;
			printk("\t\t\t******** getting taint %d on package %s\n", uobj->tag, uobj->name);
			if (!tmp->persist){
				list_del(&tmp->list);
				kfree(tmp->name);
				kfree(tmp);
			}
			return;
		}
	}
	uobj->tag = 0;
}

// update a file tag
void set_file_tag(struct object_t_u *uobj)
{
}

// Retrieve a file taint
void get_file_tag(struct object_t_u *uobj)
{
}



// store the list of apps with taint and context
void set_app_tag(struct object_t_u *uobj)
{
	//char *name;//, *del;
	//int retval;
	struct list_head *pos;
	struct object_t *tmp;
	struct object_t * a = (struct object_t *) kmalloc(sizeof(struct object_t), GFP_KERNEL);
	a->tag = uobj->tag;
	a->name = (char*) kmalloc(strlen(uobj->name) +1, GFP_KERNEL);
	strncpy(a->name, uobj->name, strlen(uobj->name) +1);
	a->idcontext = uobj->idcontext;
	/*del = strsep(&msg, " ");
	name = (char*) kmalloc(strlen(del), GFP_KERNEL);
	*(del-1) = '\0'; // not usefull, kstrtoint should check if the char is relevant for an int conversion
	retval = kstrtoint(msg, 10, &a->tag);
	strncpy(name, del, strlen(del));
	a->name = name;
	*/
	printk("\t\t\t******** Package %s can run with taint %d on context %lli\n", a->name, a->tag, (long long int) a->idcontext);

	list_for_each(pos, &app->list){
		tmp = list_entry(pos, struct object_t, list);
		if (! strcmp(uobj->name, tmp->name)){
			// A matching application, shouldn't happen
			tmp->tag = a->tag;
			kfree(a->name);
			kfree(a);
			return;
		}
	}
	list_add (&a->list, &app->list);
}

// Retrieve the context for an app with a taint
void get_app_tag(struct object_t_u *uobj)
{
	struct list_head *pos;
	struct object_t *tmp;

	printk("\t\t\t******** Looking up package %s taint %d\n", uobj->name, uobj->tag);
	list_for_each(pos, &app->list){
		tmp = list_entry(pos, struct object_t, list);
		if (! strcmp(uobj->name, tmp->name) && uobj->tag == tmp->tag){
			// A matching application
			uobj->idcontext = tmp->idcontext;
			printk("\t\t\t******** Package %s taint %d runs in context %lli\n", uobj->name, uobj->tag, (long long int) uobj->idcontext);
			list_del(&tmp->list);
			kfree(tmp->name);
			kfree(tmp);
			return;
		}
	}
	uobj->tag = 0;
}

void update_whitelist(int pid)
{
	struct whitelist_entry *e = (struct whitelist_entry*) kmalloc(sizeof(struct whitelist_entry), GFP_KERNEL);
	e->pid = pid;
	printk("\t\t\t******** Whitelisting pid %d\n", e->pid);

	list_add (&e->list, &wl->list);
}

int lookup_whitelist(int pid)
{
	struct list_head *pos;
	struct whitelist_entry *tmp;

	list_for_each(pos, &wl->list){
		tmp = list_entry(pos, struct whitelist_entry, list);
		if (tmp->pid ==  pid){
			// A matching application
			printk("\t\t\t******** PID %d is part of the whitelist\n", tmp->pid);
			return 1;
		}
	}
	// no entry
	return 0; // not in the whitelist, taint it!
}
/*
 *	Updates the context list based on the type of transition:
 */
void update_context_list(struct context_t_u *uc, unsigned int cmd){
	//enum TAG_RESPONSE r;
	struct list_head * pos;
	struct context_t * tmp;

	switch(cmd){
		case SWIRLS_CONTEXT_TIMEFENCE_ENTER:
			#ifdef SWIRLS_DEBUG
			printk("Swirls: Entered time-fence of context %lli\n", (long long int) uc->idcontext);
			#endif
			list_for_each(pos, &ctx->list){
				tmp = list_entry(pos, struct context_t, list);
				if(tmp->idcontext == uc->idcontext){
					tmp->timeFenceStatus = INSIDE_TIMEFENCE;
					break;
				}
			}
			#ifdef SWIRLS_DEBUG
			printk("\t\t\tSwirls: Context %lli NOT FOUND!\n", (long long int) uc->idcontext);
			#endif
			break;
		case SWIRLS_CONTEXT_TIMEFENCE_EXIT:
			#ifdef SWIRLS_DEBUG
			printk("Swirls: Exited time-fence of context %lli\n", (long long int) uc->idcontext);
			#endif
			list_for_each(pos, &ctx->list){
				tmp = list_entry(pos, struct context_t, list);
				if(tmp->idcontext == uc->idcontext){
					tmp->timeFenceStatus = OUTSIDE_TIMEFENCE;
				}
			}
			#ifdef SWIRLS_DEBUG
			printk("\t\t\tSwirls: Context %lli NOT FOUND!\n", (long long int) uc->idcontext);
			#endif
			break;
		case SWIRLS_CONTEXT_GEOFENCE_ENTER:
			#ifdef SWIRLS_DEBUG
			printk("Swirls: Entered geo-fence of context %lli\n", (long long int) uc->idcontext);
			#endif
			list_for_each(pos, &ctx->list){
				tmp = list_entry(pos, struct context_t, list);
				if(tmp->idcontext == uc->idcontext){
					tmp->geoFenceStatus = INSIDE_GEOFENCE;
				}
			}
			#ifdef SWIRLS_DEBUG
			printk("\t\t\tSwirls: Context %lli NOT FOUND!\n", (long long int) uc->idcontext);
			#endif
			break;
		case SWIRLS_CONTEXT_GEOFENCE_EXIT:
			#ifdef SWIRLS_DEBUG
			printk("Swirls: Exited geo-fence of context %lli\n", (long long int) uc->idcontext);
			#endif
			list_for_each(pos, &ctx->list){
				tmp = list_entry(pos, struct context_t, list);
				if(tmp->idcontext == uc->idcontext){
					tmp->geoFenceStatus = OUTSIDE_GEOFENCE;
				}
			}
			#ifdef SWIRLS_DEBUG
			printk("\t\t\tSwirls: Context %lli NOT FOUND!\n", (long long int) uc->idcontext);
			#endif
			break;
		default:
			printk("Error Updating Context list: Can't find Command %i\n", cmd);
	}
}
/*
 *	The following function is called when a geo-fence or a
 *	time-fence is exited.  This function cycles through all
 *  the current processes in the system and carries out the
 *  OOC_ACTION associated with any processes whose contextID
 *  matches that of the transitioned context.
 */
void enforce_ooc_action(struct context_t_u *uc){
	/* Set up the anchor point process*/
	struct task_struct *task = &init_task;
	/*
	 * Walk through the list of processes (tasks), checking
	 * the processes' context id's. If the id matches, carry
	 * out the OOC_ACTION
	 */
	 do{
		 if(task->idcxt == uc->idcontext){
			 switch(uc->outofcontextaction){
				case OOC_BLOCK:
					#ifdef SWIRLS_DEBUG
					printk("Swirls OOC: OOC_BLOCK pid %i",task->pid);
					#endif
					/*if(kill(task->pid, SIGKILL)<0){
						printk("Swirls: ERROR KILLING PID %i",task->pid);
					}*/
					//force_sig(SIGKILL, task);
					break;
				case OOC_ALLOW:
					#ifdef SWIRLS_DEBUG
					printk("Swirls OOC: OOC_ALLOW pid %i",task->pid);
					#endif
					break;
				case OOC_ALLOW_LOG:
					#ifdef SWIRLS_DEBUG
					printk("Swirls OOC: OOC_ALLOW_LOG pid %i",task->pid);
					#endif
					break;
				case OOC_SUPPRIM:
					#ifdef SWIRLS_DEBUG
					printk("Swirls OOC: OOC_SUPPRIM pid %i",task->pid);
					#endif
					break;
				/*case OOC_ENCRYPT:
					#ifdef SWIRLS_DEBUG
					printk("Swirls OOC: OOC_ENCRYPT pid %i",task->pid);
					#endif
					break;*/
				default:
					printk("Swirls: Error- Inavlid OOC_ACTION %d\n",uc->outofcontextaction);
					break;
			 }
		 }
	 }
	 while ( (task = next_task(task)) != &init_task);
}

void set_socket_tag(struct object_t_u *uobj)
{
	struct object_t *s = (struct object_t*) kmalloc(sizeof(struct object_t), GFP_KERNEL);
	s->tag = uobj->tag;
	s->name = (char*) kmalloc(strlen(uobj->name) +1, GFP_KERNEL);
	strncpy(s->name, uobj->name, strlen(uobj->name) +1);
	s->idcontext = uobj->idcontext;
	/*del = strsep(&msg, " ");
	name = (char*) kmalloc(strlen(del), GFP_KERNEL);
	*(del-1) = '\0'; // not usefull, kstrtoint should check if the char is relevant for an int conversion
	retval = kstrtoint(msg, 10, &a->tag);
	strncpy(name, del, strlen(del));
	a->name = name;
	*/
	printk("\t\t\t******** Setting taint %d on socket %s\n", s->tag, s->name);

	list_add (&s->list, &sock->list);
}


void get_socket_tag(struct object_t_u *uobj)
{
	struct list_head *pos;
	struct object_t *tmp;

	printk("\t\t\t******** getting taint for socket %s\n", uobj->name);
	list_for_each(pos, &sock->list){
		tmp = list_entry(pos, struct object_t, list);
		if (! strncmp(uobj->name, tmp->name, strlen(uobj->name))){ // XXX bad: should be an exact comparison and should use something else than SSL socket cname
			// A matching application
			uobj->tag = tmp->tag;
			printk("\t\t\t******** getting taint %d for socket %s\n", uobj->tag, uobj->name);
			return;
		}
	}
	// no entry, set taint 0
	uobj->tag = 0;



//	if (system_ready)
//	{
//		int retval;
//		down(&tag_sem_kernel);
//		//printk("\t\t\tMAY_OPEN: lock down\n");
//		//printk("\t\t\topen step 1\n");
//		//while (!down_trylock(&tag_sem_kernel))
//		//	wake_up_interruptible(&tag_poll);
//		//printk("\t\t\topen step 2: lock down\n");
//		sprintf(msg, "S %s", uobj->name);
//		//printk("\t\t\topen step 2: msg printed\n");
//		response_received = 0;
//		request_pending = 1;
//		wake_up_interruptible(&tag_poll);
//		//printk("\t\t\topen step 2: poll waked up\n");
//		wait_event_interruptible(tag_wq, (request_pending == 0));
//		//printk("\t\t\topen step 2: read complete\n");
//		/* wait for the reply */
//		wake_up_interruptible(&tag_poll);
//		//printk("\t\t\topen step 3: poll waked up\n");
//		while (response_received != 1) //working
//			schedule();		 //working
//		//wait_event_interruptible(tag_wq, (response_received == 1));
//		//printk("\t\t\topen step 3: response received\n");
//		retval = kstrtoint(msg, 10, &uobj->tag);
//		//printk("\t\t\topen step 4: lock up, taint %i\n", retval);
//		up(&tag_sem_kernel);
//		//printk("\t\t\tMAY_OPEN: lock up\n");
//		if (retval){
//			printk("kstrtoint returned: %i\n", retval);
//			//kfree(path);
//			//goto tag_return;
//			return -1;
//		}
//	}
//	else
//	{
//		uobj->tag = 0;
//	}
}


int get_task_struct_cmdline(struct task_struct *task, char * buffer)
{
	int res = 0;
	unsigned int len;
	struct mm_struct *mm = get_task_mm(task);
	if (!mm)
		goto out;
	if (!mm->arg_end)
		goto out_mm;	/* Shh! No looking before we're done */

	len = mm->arg_end - mm->arg_start;

	if (len > PAGE_SIZE)
		len = PAGE_SIZE;

	res = access_process_vm(task, mm->arg_start, buffer, len, 0);

	// If the nul at the end of args has been overwritten, then
	// assume application is using setproctitle(3).
	if (res > 0 && buffer[res-1] != '\0' && len < PAGE_SIZE) {
		len = strnlen(buffer, res);
		if (len < res) {
		    res = len;
		} else {
			len = mm->env_end - mm->env_start;
			if (len > PAGE_SIZE - res)
				len = PAGE_SIZE - res;
			res += access_process_vm(task, mm->env_start, buffer+res, len, 0);
			res = strnlen(buffer, res);
		}
	}
out_mm:
	mmput(mm);
out:
	return res;
}

int kill_and_assign_next_taint(struct task_struct *t, int tag)
{
	int res = 0;
	struct object_t_u uobj;
	char * cmd = (char *) kmalloc(2*PAGE_SIZE, GFP_KERNEL);
	get_task_struct_cmdline(t, cmd);
	uobj.name = cmd;
	uobj.tag = tag;
	set_next_process_taint(&uobj);
	force_sig(SIGKILL, t);
	return res;
}

// policy decision module
int checkpolicy(int tag1, int tag2, enum TAG_OPERATION action)
{
	enum TAG_RESPONSE r;
	struct list_head *pos;
	struct policy_t *tmp;

	if (tag1 == tag2) { return TAG_ALLOW; }

	// by default we don't have any policy to apply
	//r = TAG_ALLOW_LOG;
	r = TAG_BLOCK;

	list_for_each(pos, &pol->list){
		tmp = list_entry(pos, struct policy_t, list);
		if ( ( (int)(tmp->from) == tag1 && (int)(tmp->to) == tag2 ) ||
		( (int)(tmp->from) == tag2 && (int)(tmp->to) == tag1) ) {
			printk("********** checkpolicy: Matching kernel policy between capsule tag1 = %d, tag2 = %d\n", tag1, tag2);
			// A matching policy
			return tmp->action;
		}
	}

// The commented code that follow is a basic implementation, which is static

    // don't block colored apps calls to non tainted apps
//    if (!tag1 && !tag2){
//        r = TAG_ALLOW;
//        goto return_pol;
//    }

    // don't block colored apps calls to non tainted apps
//    if (tag1 == tag2){
//        r = TAG_ALLOW_LOG;
//        goto return_pol;
//    }

    // calling process with a color, receiver not tainted
    if (tag1 && !tag2){
        r = TAG_ALLOW;
        goto return_pol;
    }

    if (!tag1 && tag2){
        r = TAG_ALLOW;
        goto return_pol;
    }

    if (tag1 != tag2){
        r = TAG_BLOCK;
        goto return_pol;
    }
return_pol:
    return r;
}

struct open_file* lookup_open_file(struct file *f)
{
	struct list_head *pos;
	struct open_file *tmp;
	list_for_each(pos, &ofs->list){
		tmp = list_entry(pos, struct open_file, list);
		if (tmp->inode == f->f_dentry->d_inode && !strcmp(tmp->mnt_devname, f->f_vfsmnt->mnt_devname)){
			// it is the exact same file
			return tmp;
		}
	}
	return NULL;
}

int request_remove_file(struct path *path, struct dentry *dentry)
{
	if (system_ready && current->pid != taintctrl_pid) {
		//printk("\t\t\tDELETE: lock going down\n");
		down(&tag_sem_kernel);
		//printk("\t\t\tDELETE: lock down\n");
		sprintf(msg, "D %s%s", tomoyo_realpath_from_path(path), dentry->d_name.name);
		request_pending = 1;
		wake_up_interruptible(&tag_poll);
		wait_event_interruptible(tag_wq, (request_pending == 0));
		memset(msg, 0, TAG_MSG_SIZE);
		up(&tag_sem_kernel);
		//printk("\t\t\tDELETE: lock up\n");
	}
	return 0;
}


char * unlink_tainted(char* pathname, char* pathname_tainted){

	int idcap = gettaint(current->pid);
	if (idcap){
		sprintf(pathname_tainted, "%s_%i", pathname, idcap);
		printk("\t\t\t%s\n",pathname_tainted);
		return pathname_tainted;
	}
	else
		return pathname;
}


int cp_on_write(char* filename, int flags, int mode)
{
	struct file *file, *file_taint;
	int err = 0;
	//int kerr;
	int idcap;
	int loc_status = system_ready;
	//int nflags = flags;
	char *filename_taint;

	if (filename == NULL){
		printk("\t\t\tNULL filename\n");
		return 0;
	}

	//if ( (! strncmp(filename, "/data", 5)) && (strncmp(filename, "/data/dalvik-cache", 17)) )
	//	printk("\t\t\t%s\n",filename);

	idcap = gettaint(current->pid);
	if (idcap != 0) {

		// we allow reading untainted files
		//if (!(flags & O_WRONLY) && !(flags & O_RDWR)){
		//	goto out;
		//}

		if ( (! strncmp(filename, "/data", 5)) && (strncmp(filename, "/data/dalvik-cache", 17)) ){
			// Skip directories!

			//printk("\t\t\tcp_on_write: lock going down\n");
			down(&tag_sem_kernel);
			//printk("\t\t\tcp_on_write: lock down\n");
			system_ready = 0;
			if (!IS_ERR(file = filp_open(filename, O_DIRECTORY, mode))){
				printk("\t\t\tINFO skipping directory %s\n",filename);
				filp_close(file, NULL);
				goto out;
			}
			// check existance of a tainted file
			filename_taint = (char *) kmalloc(strlen(filename) + 4, GFP_KERNEL); // some space for the taint...
			sprintf(filename_taint, "%s_%i", filename, idcap);


//			if (nflags & O_CREAT)
//				nflags = nflags ^ O_CREAT;
//			if (nflags & O_EXCL)
//				nflags = nflags ^ O_EXCL;
//
			//file_taint = filp_open(filename_taint, nflags, mode);
			file_taint = filp_open(filename_taint, O_RDONLY|O_LARGEFILE, mode);
			//if (IS_ERR(file_taint) || file_taint == NULL)
			//printk("\t\t\tcp_on_write test results Filename: %s - NULL: %i",filename_taint, (file_taint == NULL)?0:1);
			//if (file_taint != NULL)
			//	printk(" - IS_ERR: %li\n", IS_ERR(file_taint));
			//else
			//	printk("\n");

			if (file_taint == NULL || IS_ERR(file_taint)) { //file doesn't exist
				//copy file
				//printk("\t\t\tentering copy loop for %s\n",filename);
				file = filp_open(filename, O_RDONLY|O_LARGEFILE, 0);
				if (file == NULL || IS_ERR(file)){
					printk("\t\t\tERROR error while opening %s\n",filename);
					if (flags & O_CREAT || flags & O_EXCL) { // file was going to be created anyway
						printk("\t\t\tSOLVED: creating file anyway, flags are ok\n");
						filename = krealloc(filename, strlen(filename_taint), GFP_KERNEL);
						sprintf(filename, "%s", filename_taint);
					}
					goto out;
				}
				file_taint = filp_open(filename_taint, O_WRONLY|O_CREAT|O_LARGEFILE, 0777);
				if (IS_ERR(file_taint) || file_taint == NULL){
					filp_close(file, NULL);
					printk("\t\t\tFATAL ERROR while opening %s\n",filename_taint);
					goto out;
				}
				//long do_tee(struct file *in, struct file *out, size_t len, unsigned int flags)
				//long size = do_tee(struct file *in, struct file *out, size_t len, unsigned int flags)
//XXX: Correct the Copy on write function below. It shoudl use do_tee. on file descriptors and not file structures
/*                if ( (file->f_dentry != NULL) && (file->f_dentry->d_inode != NULL) && (file->f_dentry->d_inode->i_size != NULL) ){
                    printk("\t\t\tcopy %s to  %s\n",filename, filename_taint);
                    //do_tee(file, file_taint, file->f_dentry->d_inode->i_size, SPLICE_F_NONBLOCK);
                }
                else {
                    printk("\t\t\tERROR NULL pointer on the file structures for: %s\n",filename );
                    err =1;
                }
*/
				filp_close(file, NULL);
				filp_close(file_taint, NULL);
				if (err){
					//sys_unlink(filename_taint);
					printk("\t\t\tINFO unlinking %s\n",filename_taint);
				}
			}
			else {
				printk("\t\t\tSUCCESS while opening %s\n",filename_taint);
				filp_close(file_taint, NULL);
			}

			///if(!err) {
				filename = krealloc(filename, strlen(filename_taint), GFP_KERNEL);
				sprintf(filename, "%s", filename_taint);
			//}
			goto out;
		}
	}
	else
	{
		return 0;
	}
	//printk("\t\t\t%s\n",filename);
out:
	system_ready = loc_status;
	up(&tag_sem_kernel);
	//printk("\t\t\tcp_on_write: lock up\n");
	return 0;
}

// TODO: comment the return of this function.
// 0 would mean a success
int request_file_idpol(struct file *file, int read_write)
{
	int file_idcap;
	int process_idcap;
	int retval;
	struct open_file *cur_of = NULL;
	char *path;
	char rw;
	struct timeval t0,t1;
	int temp;

	/* Cannot work at startup when the partitions are not mounted && don't consider files accessed from taintctrl: dead lock*/
	if (system_ready && current->pid != taintctrl_pid) {
		process_idcap = gettaint(current->pid);
		switch (read_write){
			case MAY_READ: // read()
				cur_of = lookup_open_file(file);
				file_idcap = (cur_of) ? cur_of->pol.idcapsule : 0;
				if (file_idcap) {
					if (!process_idcap)
						settaint(current->pid, file_idcap);
					else
						goto tag_return;
				}
				goto tag_return;
				break;

			case MAY_WRITE: // write()
				if (process_idcap) {
					cur_of = lookup_open_file(file);
					if (!cur_of){
						cur_of = (struct open_file *) kmalloc(sizeof(struct open_file), GFP_KERNEL);
						memset(cur_of, 0, sizeof(struct open_file));
					}
					file_idcap = (cur_of) ? cur_of->pol.idcapsule : 0;
					if (process_idcap != file_idcap){
						if (!file_idcap) {// no file_tag
							cur_of->pol.idcapsule = process_idcap;
							cur_of->has_changed = 1;
						}
						else
							goto tag_return;
					}
				}
				goto tag_return;
				break;

			case MAY_OPEN: //open()
				//printk("MAY OPEN\n");
				path = tomoyo_realpath_from_path(&(file->f_path));
				/* For every data file */
				if (path && ! strncmp(path, "/data", 5)) {
					//printk("\t\t\tMAY_OPEN: lock going down\n");
					down(&tag_sem_kernel);
					//printk("\t\t\tMAY_OPEN: lock down\n");
					do_gettimeofday(&t0);
					//printk("\t\t\topen step 1\n");
					//while (!down_trylock(&tag_sem_kernel))
					//	wake_up_interruptible(&tag_poll);
					//printk("\t\t\topen step 2: lock down\n");
					rw = (file->f_mode & FMODE_WRITE || file->f_mode & FMODE_PWRITE )?'W':'R';
					sprintf(msg, "R %c %s", rw, path);
					//printk("\t\t\topen step 2: msg printed\n");
					response_received = 0;
					request_pending = 1;
					wake_up_interruptible(&tag_poll);
					//printk("\t\t\topen step 2: poll waked up\n");
					wait_event_interruptible(tag_wq, (request_pending == 0));
					//printk("\t\t\topen step 2: read complete\n");
					// wait for the reply
					wake_up_interruptible(&tag_poll);
					//printk("\t\t\topen step 3: poll waked up\n");
					while (response_received != 1) //working
						schedule();		 //working
					//wait_event_interruptible(tag_wq, (response_received == 1));
					//printk("\t\t\topen step 3: response received\n");
					retval = kstrtoint(msg, 10, &file_idcap);
					//printk("\t\t\topen step 4: lock up, taint %i\n", retval);
					up(&tag_sem_kernel);
					//printk("\t\t\tMAY_OPEN: lock up\n");
					if (retval){
						printk("kstrtoint returned: %i\n", retval);
						kfree(path);
						goto tag_return;
						return retval;
					}
					cur_of = lookup_open_file(file);
					if (!cur_of){
						cur_of = (struct open_file *) kmalloc(sizeof(struct open_file), GFP_KERNEL);
						cur_of->path = tomoyo_realpath_from_path(&(file->f_path));
						//cur_of->path = path;
						cur_of->count = 1;
						cur_of->inode = file->f_dentry->d_inode;
						cur_of->mnt_devname = file->f_vfsmnt->mnt_devname;
						cur_of->pol.idcapsule = 0;
						list_add (&cur_of->list, &ofs->list);
						//printk("process: %i Adding PATH: %s, DEVNAME: %s, ANSWER: %i\n",
						//		current->pid, path, cur_of->mnt_devname,
						//		(cur_of->pol.idcapsule) ? cur_of->pol.idcapsule : 0);
					}
					else
						cur_of->count ++;

					kfree(path);
					do_gettimeofday(&t1);
					temp = (int) (t1.tv_sec - t0.tv_sec)*1000 + (t1.tv_usec - t0.tv_usec)/1000;
					printk("\t\t\topen time : %i\n", temp);
				}
				goto tag_return;
				break;

			case 3: // close()
				// remove path from list
				cur_of = lookup_open_file(file);
				if (cur_of) {
					//printk("\t\t\tMAY_CLOSE: lock going down\n");
					down(&tag_sem_kernel);
					//printk("\t\t\tMAY_CLOSE: lock down\n");
					//printk("\t\t\tclose step 1\n");
					if (cur_of->count == 1){
						printk("process: %i Removing PATH: %s, DEVNAME: %s, IDCAP: %i\n",
								current->pid, cur_of->path, cur_of->mnt_devname,
								(cur_of->pol.idcapsule) ? cur_of->pol.idcapsule : 0);
						if (cur_of->pol.idcapsule && cur_of->has_changed){ // Update database
							//printk("\t\t\tclose step 2: lock down\n");
							//while(!down_trylock(&tag_sem_kernel))
							//	wake_up_interruptible(&tag_poll);
							sprintf(msg, "W %i %s", cur_of->pol.idcapsule, cur_of->path);
							request_pending = 1;
							wake_up_interruptible(&tag_poll);
							wait_event_interruptible(tag_wq, (request_pending == 0));
							memset(msg, 0, TAG_MSG_SIZE);
							//printk("\t\t\tclose step 3: lock up\n");
						}
						list_del(&cur_of->list);
						kfree(cur_of->path);
						kfree(cur_of);
					}
					else
						cur_of->count --;
					up(&tag_sem_kernel);
					//printk("\t\t\tMAY_CLOSE: lock up\n");
				}
				goto tag_return;
				break;

			default:
				break;
		}
	}
	else {
		// printk("PATH: %s ACTION: %i\n", path, read_write);
	}
	return 0;

tag_return:
	return 0;
}
EXPORT_SYMBOL(request_file_idpol);

long device_ioctl(struct file *filep, unsigned int cmd, unsigned long arg) {
	//int len = TAG_MSG_SIZE - 1;
	int retval=0;
	int err=0;
	int tag[3]; // for eval policy, gettaint, settaint
	int pid;
	struct capsule_t_u ucap;
	struct policy_t_u upol;
	struct context_t_u uctx;
	struct object_t_u uobj;

	/* Controls checks ref: Linux Device Drivers, 3rd ed */
        /*
         * extract the type and number bitfields, and don't decode
         * wrong cmds: return ENOTTY (inappropriate ioctl) before access_ok()
         */
        if (_IOC_TYPE(cmd) != SWIRLS_IOC_MAGIC) return -ENOTTY;
        if (_IOC_NR(cmd) > SWIRLS_IOC_MAXNR) return -ENOTTY;

        /*
         * the direction is a bitmask, and VERIFY_WRITE catches R/W
         * transfers. `Type' is user-oriented, while
         * access_ok is kernel-oriented, so the concept of "read" and
         * "write" is reversed
         */
        if (_IOC_DIR(cmd) & _IOC_READ)
                err = !access_ok(VERIFY_WRITE, (void __user *)arg, _IOC_SIZE(cmd));
        else if (_IOC_DIR(cmd) & _IOC_WRITE)
                err =  !access_ok(VERIFY_READ, (void __user *)arg, _IOC_SIZE(cmd));
        if (err) return -EFAULT;

	//if (cmd != SWIRLS_EVAL_POLICY)
	//printk("IOCTL on Swirls: %d\n", cmd);

	switch(cmd) {
		// Userspace reading data from SWIRLS module
		case SWIRLS_READ:
			down(&tag_sem_user);
			retval = copy_to_user((char *)arg, msg, strlen(msg) +1);
			if (retval) {
				retval = -EFAULT;
				goto error;
			}
			request_pending = 0;
			wake_up_interruptible(&tag_wq);
			memset(msg, 0, TAG_MSG_SIZE);
			up(&tag_sem_user);
			break;

		// Userspace writing data to SWIRLS module
		case SWIRLS_WRITE:
			down(&tag_sem_user);
			retval = copy_from_user(msg, (char *)arg, strlen((char *)arg)+1);
			if (retval) {
				retval = -EFAULT;
				goto error;
			}
			response_received = 1;
			//printk("\t\t\tResponse received!\n");
			wake_up_interruptible(&tag_wq);
			up(&tag_sem_user);
			break;

		// Start or stop the file tracking system
		case SWIRLS_SYSTEM:
			down(&tag_sem_user);
			if(! capable(CAP_SYS_ADMIN)){
				retval = -EPERM;
				goto error;
			}
			if (!system_ready){
				taintctrl_pid = current->pid;
				system_ready = 1;
				printk("Starting to record file access\n");
			}
			else {
				system_ready = 0;
				printk("File access recording stopped\n");
			}
			up(&tag_sem_user);
			break;

		// Copy a capsule information from userspace
		case SWIRLS_CAPSULE:
			down(&tag_sem_user);
			retval = copy_from_user(&ucap, (struct capsule_t_u *)arg, sizeof(struct capsule_t_u));
			if (retval) {
				retval = -EFAULT;
				goto error;
			}
			printk("Adding to capsule entry id: %li\n",(long int) ucap.idcapsule);
			add_to_capsule(&ucap);
			up(&tag_sem_user);
			break;

		//Copy a context information from userspace
		case SWIRLS_CONTEXT_ADD:
			down(&tag_sem_user);
			retval = copy_from_user(&uctx, (struct context_t_u *)arg, sizeof(struct context_t_u));
			if (retval) {
				retval = -EFAULT;
				goto error;
			}
			printk("Adding to context entry id: %li\n", (long int) uctx.idcontext);
			add_to_context(&uctx);
			up(&tag_sem_user);
			break;

		// Copy policy information from userspace
		case SWIRLS_POLICY:
			down(&tag_sem_user);
			retval = copy_from_user(&upol, (struct policy_t_u *)arg, sizeof(struct policy_t_u));
			if (retval) {
				retval = -EFAULT;
				goto error;
			}
			printk("Adding to policy entry from: %li to: %li action: %i\n",(long int) upol.from, (long int) upol.from, upol.action);
			add_to_policy(&upol);
			up(&tag_sem_user);
			break;

		// request pid taint
		case SWIRLS_GETTAINT:
			down(&tag_sem_user);
			retval = copy_from_user(tag, (int *)arg, sizeof(int));
			if (retval == 0)
				tag[0] = gettaint(tag[0]);
				retval = copy_to_user((int *)arg, tag, sizeof(int));
			if (retval) {
				retval = -EFAULT;
				goto error;
			}
			up(&tag_sem_user);
			break;

		// Assign pid taint
		case SWIRLS_SETTAINT:
			down(&tag_sem_user);
			retval = copy_from_user(tag, (int *)arg, 2*sizeof(int));
			if (retval == 0)
				tag[0] = settaint(tag[0], tag[1]);
				retval = copy_to_user((int *)arg, tag, sizeof(int));
			if (retval) {
				retval = -EFAULT;
				goto error;
			}
			up(&tag_sem_user);
			break;

		// Evaluate the policy according two given taints
		case SWIRLS_EVAL_POLICY:
			down(&tag_sem_user);
			retval = copy_from_user(tag, (int *)arg, 3*sizeof(int));
			if (retval == 0){
				//printk("checkpolicy on taint %d and %d for action %d", tag[0], tag[1], tag[2]);
				tag[0] = checkpolicy(tag[0], tag[1], tag[2]);
				//printk(" returning %d\n", tag[0]);
				retval = copy_to_user((int *)arg, tag, sizeof(int));
			}
			if (retval) {
				retval = -EFAULT;
				goto error;
			}
			up(&tag_sem_user);
			break;

		case SWIRLS_SET_NEXT_TAG:
			down(&tag_sem_user);
			retval = copy_from_user(&uobj, (struct object_t_u *)arg, sizeof(struct object_t_u));
			if (retval) { retval = -EFAULT; goto error; }
			//retval = copy_from_user(msg, (char *)uobj.name, len);
			retval = copy_from_user(msg, (char *)uobj.name, strlen(uobj.name) +1);
			if (retval) { retval = -EFAULT; goto error; }
			uobj.name = msg;
			if (retval == 0){
				set_next_process_taint(&uobj);
			}
			if (retval) {
				retval = -EFAULT;
				goto error;
			}
			memset(msg, 0, TAG_MSG_SIZE);
			memset(&uobj, 0, sizeof(struct object_t_u));
			up(&tag_sem_user);
			break;

		case SWIRLS_GET_NEXT_TAG:
			down(&tag_sem_user);
			retval = copy_from_user(&uobj, (struct object_t_u *)arg, sizeof(struct object_t_u));
			if (retval) { retval = -EFAULT; goto error; }
			//retval = copy_from_user(msg, (char *)uobj.name, len);
			retval = copy_from_user(msg, (char *)uobj.name, strlen(uobj.name) +1);
			if (retval) { retval = -EFAULT; goto error; }
			uobj.name = msg;
			if (retval == 0){
				get_next_process_taint(&uobj);
				retval = copy_to_user((struct object_t_u *)arg, &uobj , sizeof(struct object_t_u));
			}
			if (retval) {
				retval = -EFAULT;
				goto error;
			}
			memset(msg, 0, TAG_MSG_SIZE);
			memset(&uobj, 0, sizeof(struct object_t_u));
			up(&tag_sem_user);
			break;

		case SWIRLS_SET_SOCKET_TAG:
			down(&tag_sem_user);
			retval = copy_from_user(&uobj, (struct object_t_u *)arg, sizeof(struct object_t_u));
			if (retval) { retval = -EFAULT; goto error; }
			//retval = copy_from_user(msg, (char *)uobj.name, len);
			retval = copy_from_user(msg, (char *)uobj.name, strlen(uobj.name) +1);
			if (retval) { retval = -EFAULT; goto error; }
			uobj.name = msg;
			if (retval == 0){
				set_socket_tag(&uobj);
				//retval = copy_to_user((struct object_t_u *)arg, &uobj , sizeof(struct object_t_u));
			}
			if (retval) {
				retval = -EFAULT;
				goto error;
			}
			memset(msg, 0, TAG_MSG_SIZE);
			memset(&uobj, 0, sizeof(struct object_t_u));
			up(&tag_sem_user);
			break;

		case SWIRLS_GET_SOCKET_TAG:
			down(&tag_sem_user);
			retval = copy_from_user(&uobj, (struct object_t_u *)arg, sizeof(struct object_t_u));
			if (retval) { retval = -EFAULT; goto error; }
			//retval = copy_from_user(msg, (char *)uobj.name, len);
			retval = copy_from_user(msg, (char *)uobj.name, strlen(uobj.name)+1);
			if (retval) { retval = -EFAULT; goto error; }
			uobj.name = msg;
			if (retval == 0){
				get_socket_tag(&uobj);
				retval = copy_to_user((struct object_t_u *)arg, &uobj , sizeof(struct object_t_u));
			}
			if (retval) {
				retval = -EFAULT;
				goto error;
			}
			memset(msg, 0, TAG_MSG_SIZE);
			memset(&uobj, 0, sizeof(struct object_t_u));
			up(&tag_sem_user);
			break;

		case SWIRLS_WHITELIST_PID:
			down(&tag_sem_user);
			retval = copy_from_user(&pid, (int *) arg, sizeof(int));
			if (retval) { retval = -EFAULT; goto error; }
			update_whitelist(pid);
			up(&tag_sem_user);
			break;

		case SWIRLS_CONTEXT_TIMEFENCE_ENTER:
			//printk("\t\t\t SWIRLS_CONTEXT_TIMEFENCE_ENTER: lock down\n");
			down(&tag_sem_user);
			retval = copy_from_user(&uctx, (struct context_t_u*)arg, sizeof(struct context_t_u));
			if (retval) {
				retval = -EFAULT;
				goto error;
			}
			update_context_list(&uctx,SWIRLS_CONTEXT_TIMEFENCE_ENTER);
			up(&tag_sem_user);
			break;

		case SWIRLS_CONTEXT_TIMEFENCE_EXIT:
			//printk("\t\t\t SWIRLS_CONTEXT_TIMEFENCE_EXIT: lock down\n");
			down(&tag_sem_user);
			retval = copy_from_user(&uctx, (struct context_t_u*)arg, sizeof(struct context_t_u));
			if (retval) {
				retval = -EFAULT;
				goto error;
			}
			update_context_list(&uctx, SWIRLS_CONTEXT_TIMEFENCE_EXIT);
			enforce_ooc_action(&uctx);
			up(&tag_sem_user);
			break;

		case SWIRLS_CONTEXT_GEOFENCE_ENTER:
			//printk("\t\t\t SWIRLS_CONTEXT_GEOFENCE_ENTER: lock down\n");
			down(&tag_sem_user);
			retval = copy_from_user(&uctx, (struct context_t_u *)arg, sizeof(struct context_t_u));
			if (retval) {
				retval = -EFAULT;
				goto error;
			}
			update_context_list(&uctx,SWIRLS_CONTEXT_GEOFENCE_ENTER);
			up(&tag_sem_user);
			break;

		case SWIRLS_CONTEXT_GEOFENCE_EXIT:
			//printk("\t\t\t SWIRLS_CONTEXT_GEOFENCE_EXIT: lock down\n");
			down(&tag_sem_user);
			retval = copy_from_user(&uctx, (struct context_t_u *)arg, sizeof(struct context_t_u));
			if (retval) {
				retval = -EFAULT;
				goto error;
			}
			update_context_list(&uctx,SWIRLS_CONTEXT_GEOFENCE_EXIT);
			enforce_ooc_action(&uctx);
			up(&tag_sem_user);
			break;

		case SWIRLS_SET_APP_TAG:
			down(&tag_sem_user);
			retval = copy_from_user(&uobj, (struct object_t_u *)arg, sizeof(struct object_t_u));
			if (retval) { retval = -EFAULT; goto error; }
			//retval = copy_from_user(msg, (char *)uobj.name, len);
			retval = copy_from_user(msg, (char *)uobj.name, strlen(uobj.name) +1);
			if (retval) { retval = -EFAULT; goto error; }
			uobj.name = msg;
			if (retval == 0){
				set_app_tag(&uobj);
			}
			if (retval) {
				retval = -EFAULT;
				goto error;
			}
			memset(msg, 0, TAG_MSG_SIZE);
			memset(&uobj, 0, sizeof(struct object_t_u));
			up(&tag_sem_user);
			break;

		case SWIRLS_GET_APP_TAG:
			down(&tag_sem_user);
			retval = copy_from_user(&uobj, (struct object_t_u *)arg, sizeof(struct object_t_u));
			if (retval) { retval = -EFAULT; goto error; }
			//retval = copy_from_user(msg, (char *)uobj.name, len);
			retval = copy_from_user(msg, (char *)uobj.name, strlen(uobj.name) +1);
			if (retval) { retval = -EFAULT; goto error; }
			uobj.name = msg;
			if (retval == 0){
				get_app_tag(&uobj);
				retval = copy_to_user((struct object_t_u *)arg, &uobj , sizeof(struct object_t_u));
			}
			if (retval) {
				retval = -EFAULT;
				goto error;
			}
			memset(msg, 0, TAG_MSG_SIZE);
			memset(&uobj, 0, sizeof(struct object_t_u));
			up(&tag_sem_user);
			break;
		case SWIRLS_GET_FILE_TAG:
			down(&tag_sem_user);
			retval = copy_from_user(&uobj, (struct object_t_u *)arg, sizeof(struct object_t_u));
			if (retval) { retval = -EFAULT; goto error; }
			//retval = copy_from_user(msg, (char *)uobj.name, len);
			retval = copy_from_user(msg, (char *)uobj.name, strlen(uobj.name) +1);
			if (retval) { retval = -EFAULT; goto error; }
			uobj.name = msg;
			if (retval == 0){
				get_file_tag(&uobj);
				retval = copy_to_user((struct object_t_u *)arg, &uobj , sizeof(struct object_t_u));
			}
			if (retval) {
				retval = -EFAULT;
				goto error;
			}
			memset(msg, 0, TAG_MSG_SIZE);
			memset(&uobj, 0, sizeof(struct object_t_u));
			up(&tag_sem_user);
			break;
		case SWIRLS_SET_FILE_TAG:
			down(&tag_sem_user);
			retval = copy_from_user(&uobj, (struct object_t_u *)arg, sizeof(struct object_t_u));
			if (retval) { retval = -EFAULT; goto error; }
			//retval = copy_from_user(msg, (char *)uobj.name, len);
			retval = copy_from_user(msg, (char *)uobj.name, strlen(uobj.name) +1);
			if (retval) { retval = -EFAULT; goto error; }
			uobj.name = msg;
			if (retval == 0){
				set_file_tag(&uobj);
			}
			if (retval) {
				retval = -EFAULT;
				goto error;
			}
			memset(msg, 0, TAG_MSG_SIZE);
			memset(&uobj, 0, sizeof(struct object_t_u));
			up(&tag_sem_user);
			break;
		default:
			retval = -ENOTTY;
	}
	return retval;

error:
	printk("IOCTL error: %i\n", retval);
	up(&tag_sem_user);
	return retval;
}

/*
static ssize_t device_read(struct file *filp, char __user *buffer, size_t length, loff_t *offset)
{
	int ret = simple_read_from_buffer(buffer, length, offset, msg, 500);
	request_pending = 0;
	return ret;
}

static ssize_t device_write(struct file *filp, const char __user *buff, size_t len, loff_t *off)
{
	int retval;
	if (len > 499)
		return -EINVAL;
	retval = copy_from_user(msg, buff, len);
	if (retval)
		return retval;
	msg[len] = '\0';
	response_received=1;
	return len;
}
*/

static unsigned int device_poll(struct file *filp, struct poll_table_struct *wait)
{
	/* writable */
	unsigned int mask = 0;
	down(&tag_sem_user);
	poll_wait(filp, &tag_poll, wait);
	if (request_pending)
		mask |= POLLIN | POLLRDNORM; /* read */
	//if (!response_received)
	//	mask |= POLLOUT | POLLWRNORM; /* write */
	up(&tag_sem_user);
	//printk("Select return: %i\n", mask);
	return mask;
}

static struct file_operations fops = {
//	.read = device_read,
//	.write = device_write,
	.poll = device_poll,
	.unlocked_ioctl = device_ioctl
};

/* Syscalls declarations */
SYSCALL_DEFINE0(loadpolicy)
{
	return 0;
}

SYSCALL_DEFINE1(gettaint, pid_t, pid)
{
	return gettaint(pid);
}

SYSCALL_DEFINE2(settaint, pid_t, pid, unsigned int, tag)
{
	return settaint(pid, tag);
}

//INIT/////////////////////////////////////////////////////////////////////////
static int __init taintctrl_init(void)
{
	dev_t dev = 0;
	init_waitqueue_head(&tag_poll);
	init_waitqueue_head(&tag_wq);
	//init_completion(&comp_r);
	//init_completion(&comp_w);
	//sema_init(&tag_sem_user, 1);
	//sema_init(&tag_sem_kernel, 1);
	msg = kmalloc(TAG_MSG_SIZE * sizeof(char), GFP_KERNEL);
	memset(msg, 0, TAG_MSG_SIZE);

	major = register_chrdev(dev, SWIRLS_DEVICE_NAME, &fops);

	if (major < 0) {
		printk ("Registering the character device failed with %d\n", major);
		return major;
	}

	class = class_create(THIS_MODULE, SWIRLS_DEVICE_NAME);
	device = device_create(class, NULL, MKDEV(major, 0), NULL, SWIRLS_DEVICE_NAME);


	printk("taintctrl device created: assigned major: %d\n", major);
	//printk("cdev example: assigned major: %d\n", major);
	//printk("create node with mknod /dev/taintctrl c %d 0\n", major);

	/* list inititalisations */
	/* open files - tag association list */
	ofs = (struct open_file *) kmalloc(sizeof(struct open_file), GFP_KERNEL);
	INIT_LIST_HEAD(&ofs->list);

	/* capsule list */
	cap = (struct capsule_t *) kmalloc(sizeof(struct capsule_t), GFP_KERNEL);
	INIT_LIST_HEAD(&cap->list);

	/* context list */
	ctx = (struct context_t *) kmalloc(sizeof(struct context_t), GFP_KERNEL);
	INIT_LIST_HEAD(&ctx->list);

	/* policy list */
	pol = (struct policy_t *) kmalloc(sizeof(struct policy_t), GFP_KERNEL);
	INIT_LIST_HEAD(&pol->list);

	/* application taint and context list */
	app = (struct object_t *) kmalloc(sizeof(struct object_t), GFP_KERNEL);
	INIT_LIST_HEAD(&app->list);

	/* application tag to set list */
	procnexttaint = (struct object_t *) kmalloc(sizeof(struct object_t), GFP_KERNEL);
	INIT_LIST_HEAD(&procnexttaint->list);

	/* socket tag list */
	sock = (struct object_t *) kmalloc(sizeof(struct object_t), GFP_KERNEL);
	INIT_LIST_HEAD(&sock->list);

	/* non taintable pids tag list */
	wl = (struct whitelist_entry *) kmalloc(sizeof(struct whitelist_entry), GFP_KERNEL);
	INIT_LIST_HEAD(&wl->list);


	return 0;
}

static void __exit taintctrl_exit(void)
{
	kfree(msg);
	device_destroy(class, MKDEV(major, 0));
	// TODO: empty lists
	class_unregister(class);
	class_destroy(class);
	unregister_chrdev(major, SWIRLS_DEVICE_NAME);
	printk("EXIT MODULE Taintctrl");
}

//MACROSES/////////////////////////////////////////////////////////////////////
module_init(taintctrl_init);
module_exit(taintctrl_exit);
MODULE_LICENSE("GPL");
