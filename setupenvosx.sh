#!/bin/sh
export PATH=/Volumes/android/taintdroid/tdroid-4.1.1_r6/prebuilt/darwin-x86/toolchain/arm-eabi-4.4.3/bin:$PATH
export ARCH=arm
export SUBARCH=arm
export CROSS_COMPILE=arm-eabi-
#export CROSS_COMPILE=arm-elf-
make -f Makefile tegra3_android_defconfig &> /dev/null
#make -j16 CONFIG_DEBUG_SECTION_MISMATCH=y CONFIG_SECURITY_TAINTCTRL=y #CONFIG_SECURITY=y
#make -j16 CONFIG_DEBUG_SECTION_MISMATCH=y
make -f Makefile -j16 CONFIG_DEBUG_SECTION_MISMATCH=y CONFIG_UNION_FS=y CONFIG_UNION_FS_XATTR=y
# cp arch/arm/boot/zImage ../tdroid-4.1.1_r6/device/samsung/crespo/kernel
