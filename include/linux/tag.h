#ifndef _LINUX_TAG_H
#define _LINUX_TAG_H

// Tomoyo defs
//char *tomoyo_realpath_from_path(struct path *path);

// Taintctrl structs and global data
enum TAG_RESPONSE {
  TAG_NONEXISTANT,
  TAG_BLOCK,
  TAG_ALLOW,
  TAG_ALLOW_LOG,
  TAG_UPDATE
};

enum GEOFENCE_STATUS{
	INSIDE_GEOFENCE,
	OUTSIDE_GEOFENCE
};

enum TIMEFENCE_STATUS{
	INSIDE_TIMEFENCE,
	OUTSIDE_TIMEFENCE
};

enum TAG_OPERATION {
  TAG_OPERATION_PROCESS,
  TAG_OPERATION_FILE,
  TAG_OPERATION_BINDER
};

enum OOC_ACTION {
  OOC_BLOCK,
  OOC_ALLOW,
  OOC_ALLOW_LOG,
  OOC_SUPPRIM
};

struct object_t {
	struct list_head list;
	int tag;
	int persist;
	int64_t idcontext;
	char *name;
};

struct capsule_t {
	struct list_head list;
	int id;
	unsigned int tag;
	char *name;
	int version;
};

struct context_t {
	struct list_head list;
	int64_t idcontext;
	enum GEOFENCE_STATUS geoFenceStatus;
	enum TIMEFENCE_STATUS timeFenceStatus;
	enum OOC_ACTION outofcontextaction;
};

struct policy_t {
	struct list_head list;
	int64_t from;
	int64_t to;
	enum TAG_RESPONSE action;
};

struct tag_policy_t {
	int idcapsule;
	int idcontext;
};

#define SWIRLS_DEVICE_NAME	"taintctrl"

#define SWIRLS_IOC_MAGIC	'G'
#define SWIRLS_SYSTEM	_IO(SWIRLS_IOC_MAGIC, 0)
#define SWIRLS_READ		_IOR(SWIRLS_IOC_MAGIC, 1, char *)
#define SWIRLS_WRITE		_IOW(SWIRLS_IOC_MAGIC, 2, char *)
#define SWIRLS_GETTAINT	_IOR(SWIRLS_IOC_MAGIC, 3, int *)
#define SWIRLS_SETTAINT	_IOW(SWIRLS_IOC_MAGIC, 4, int *)
#define SWIRLS_CAPSULE	_IOW(SWIRLS_IOC_MAGIC, 5, struct capsule_t_u *)
#define SWIRLS_CONTEXT_ADD	_IOW(SWIRLS_IOC_MAGIC, 6, struct context_t_u *)
#define SWIRLS_POLICY	_IOW(SWIRLS_IOC_MAGIC, 7, struct policy_t_u *)
#define SWIRLS_EVAL_POLICY	_IOWR(SWIRLS_IOC_MAGIC, 8, int *)
#define SWIRLS_SET_NEXT_TAG	_IOW(SWIRLS_IOC_MAGIC, 9, struct object_t_u *)
#define SWIRLS_GET_NEXT_TAG	_IOWR(SWIRLS_IOC_MAGIC, 10, struct object_t_u *)
#define SWIRLS_GET_SOCKET_TAG	_IOWR(SWIRLS_IOC_MAGIC, 11, struct object_t_u *)
#define SWIRLS_SET_SOCKET_TAG	_IOW(SWIRLS_IOC_MAGIC, 12, struct object_t_u *)
#define SWIRLS_WHITELIST_PID	_IOW(SWIRLS_IOC_MAGIC, 13, int *)
#define SWIRLS_CONTEXT_TIMEFENCE_ENTER	_IOW(SWIRLS_IOC_MAGIC, 14, struct context_t_u*)
#define SWIRLS_CONTEXT_TIMEFENCE_EXIT	_IOW(SWIRLS_IOC_MAGIC, 15, struct context_t_u*)
#define SWIRLS_CONTEXT_GEOFENCE_ENTER	_IOW(SWIRLS_IOC_MAGIC, 16, struct context_t_u*)
#define SWIRLS_CONTEXT_GEOFENCE_EXIT	_IOW(SWIRLS_IOC_MAGIC, 17, struct context_t_u*)
#define SWIRLS_SET_APP_TAG	_IOW(SWIRLS_IOC_MAGIC, 18, struct object_t_u *)
#define SWIRLS_GET_APP_TAG	_IOWR(SWIRLS_IOC_MAGIC, 19, struct object_t_u *)
#define SWIRLS_GET_FILE_TAG	_IOWR(SWIRLS_IOC_MAGIC, 20, struct object_t_u *)
#define SWIRLS_SET_FILE_TAG	_IOW(SWIRLS_IOC_MAGIC, 21, struct object_t_u *)

// MAX
#define SWIRLS_IOC_MAXNR	21

//struct tag_policy_t * get_tag_pol_from_pid(pid_t pid);
int get_tag_pol_from_pid(pid_t pid);
int gettaint(pid_t pid);
int settaint(pid_t pid, unsigned int tag);
//int checkpolicy(struct tag_policy_t *tp1, struct tag_policy_t *tp2, enum TAG_OPERATION action);
int checkpolicy(int tag1, int tag2, enum TAG_OPERATION action);
int request_remove_file(struct path *path, struct dentry *dentry);
int request_file_idpol(struct file *file, int read_write);
int cp_on_write(char* filename, int flags, int mode);
char * unlink_tainted(char* pathname, char* pathname_tainted);
int lookup_whitelist(int pid);
int kill_and_assign_next_taint(struct task_struct *t, int tag);

#endif
